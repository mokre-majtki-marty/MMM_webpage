import json
import random

import requests
from soup import create_soup


class Product:
    JSON_PATH = "../ScrapResults/json/data.json"

    def __init__(self, position: int, name: str, image: str, price_currency: str, price: float, url: str,
                 availability: str, c1: str, c2: str, c3: str) -> None:
        self.position = position
        self.name = name
        self.image = image
        self.price_currency = price_currency
        self.price = price
        self.url = url
        self.availability = availability
        self.c1 = c1
        self.c2 = c2
        self.c3 = c3
        self.thumbnails = self.get_thumbnails()
        self.save_image()
        self.save_data_to_json()

    @staticmethod
    def get_destination_path(path: str) -> str:
        return path.split("/")[-1]

    @staticmethod
    def get_destination_path_for_thumbnails(path: str) -> str:
        parts_of_path = path.split("/")
        return parts_of_path[5] + parts_of_path[-1]

    def get_thumbnails(self):
        soup = create_soup(self.url)
        thumbnails = soup.find("img", {"alt": self.name}).get("srcset").split(", ")
        thumbnails = [thumbnail[:-3] for thumbnail in thumbnails]
        return thumbnails

    def save_image(self) -> None:
        try:
            response = requests.get(self.image, stream=True)
            response.raise_for_status()

            destination_path = f"../ScrapResults/img/{self.get_destination_path(self.image)}"

            with open(destination_path, "wb") as file:
                for chunk in response.iter_content(chunk_size=8192):
                    file.write(chunk)


            for thumbnail in self.thumbnails:
                response = requests.get(thumbnail, stream=True)
                response.raise_for_status()
                destination_path = f"../ScrapResults/img/{self.get_destination_path_for_thumbnails(thumbnail)}"
                with open(destination_path, "wb") as file:
                    for chunk in response.iter_content(chunk_size=8192):
                        file.write(chunk)



        except requests.exceptions.RequestException as e:
            print(f"errort: {e}")

    def save_data_to_json(self):
        json_data = {}
        soup = create_soup(self.url)
        for li_item in soup.find_all("ul")[-1]:
            li_text = ''.join(li_item.stripped_strings).replace(":", "")
            strong_text = li_item.strong.text.replace(":", "")
            final_text = li_text.replace(strong_text, "").strip()
            json_data[strong_text] = final_text

        json_data = {"attributes": json_data, "Position": self.position, "Name": self.name,
                     "Image": self.get_destination_path(self.image), "Price_currency": self.price_currency,
                     "Price": self.price, "Url": self.url, "Availability": self.availability,
                     "Thumbnails": list(map(self.get_destination_path_for_thumbnails, self.thumbnails)),
                     "Category": self.c1, "Sub_category": self.c2, "Sub_sub_category": self.c3,
                     "Weight": float(int(self.price * (random.random() + 1)) / 200) + 0.1}

        with open(self.JSON_PATH, "a", encoding='utf-8') as file:
            json.dump(json_data, file)
            file.write(',')

    def __str__(self) -> str:
        return self.name

    @classmethod
    def create_product_from_json(cls, json_data, categories):
        return Product(
            json_data["position"],
            json_data["item"]["name"],
            json_data["item"]["image"],
            json_data["item"]["offers"]["priceCurrency"],
            json_data["item"]["offers"]["price"],
            json_data["item"]["offers"]["url"],
            json_data["item"]["offers"]["availability"],
            categories[0],
            categories[1],
            categories[2]
        )
