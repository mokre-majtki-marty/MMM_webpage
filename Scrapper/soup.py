from bs4 import BeautifulSoup
import requests


def create_soup(url: str) -> BeautifulSoup:
    html_doc = requests.get(url).text
    return BeautifulSoup(html_doc, "html.parser")
