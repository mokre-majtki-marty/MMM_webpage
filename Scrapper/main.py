import json

from Scrapper.product import Product
from soup import create_soup

from selenium import webdriver
from selenium.webdriver.chrome.service import Service as ChromeService
from selenium.webdriver.common.by import By

from time import sleep

SALE_URL = "https://nbsklep.pl/promocja"
JSON_DATA_PATH = "../ScrapResults/json/data.json"
JSON_CATEGORY_PATH = "../ScrapResults/json/category.json"
PAGE_URL = "https://nbsklep.pl"
CHROME_DRIVER = "chromeDriver/chromeDriver.exe"

number_of_products = 0
chrome_service = ChromeService(CHROME_DRIVER)
driver = webdriver.Chrome(service=chrome_service)

def get_urls(data, urls) -> None:
    for d in data:
        if "children" in d:
            get_urls(d["children"], urls)
        if "niceUrl" in d:
            urls.append('/' + d["niceUrl"])


def scan_for_products(json_data: json, categories: list[str]):
    global number_of_products
    if not len(json_data["@graph"][0]["itemListElement"]):
        return
    for item in json_data["@graph"][0]["itemListElement"]:
        Product.create_product_from_json(item, categories)
        number_of_products += 1
        print(f"Number of products: {number_of_products}")


def save_categories(categories: list[str]) -> None:
    with open(JSON_CATEGORY_PATH, "w", encoding='utf-8') as file:
        pass

    with open(JSON_CATEGORY_PATH, "w", encoding='utf-8') as file:
        file.write("[")
        categories_dic = {}
        for category in categories:
            items = category.split("/")[1:]
            if len(items) == 3:
                if items[0] not in categories_dic:
                    categories_dic[items[0]] = {}

                if items[1] not in categories_dic[items[0]]:
                    categories_dic[items[0]][items[1]] = []

                categories_dic[items[0]][items[1]].append(items[2])

        file.write(str(categories_dic))
        file.write("]")


def get_categories() -> list[str]:
    soup = create_soup(PAGE_URL)

    categories = [category.a.get('href') for category in soup.find("ul")]

    script_tag = soup.find("script", {"id": "__NEXT_DATA__", "type": "application/json"})
    script_content = script_tag.string
    json_data = \
        json.loads(script_content)["props"]["pageProps"]["dehydratedState"]["queries"][3]["state"]["data"]["menu"][
            "children"]

    url = []
    get_urls(json_data, url)
    categories.extend(url)

    return categories


def clear_json() -> None:
    with open(JSON_DATA_PATH, "w", encoding='utf-8') as file:
        pass


clear_json()
categories = get_categories()[9:]
save_categories(categories)
for category in categories:
    if category == "/running":
        continue
    else:
        url = f"{PAGE_URL}{category}"
        print(url)

        driver.get(url)
        sleep(2)

        try:
            script_element = driver.find_element(By.XPATH, "//script[@type='application/ld+json']")
            try:
                script_content = script_element.get_attribute("innerHTML")
            except:
                print("Can import html")

            json_data = json.loads(script_content)
            print(json_data)
        except json.JSONDecodeError as e:
            print(f"Error decoding JSON: {e}")
        try:
            scan_for_products(json_data, category.split('/')[1:])
        except Exception as e:
            print(e)