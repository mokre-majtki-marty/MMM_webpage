# MMM_webpage

## Installation on Windows

### Prequirements
 - Basic knowledge of Linux CLI commands
 - Internet connection

 1. [Install Ubuntu using WSL](https://learn.microsoft.com/en-us/windows/wsl/install)
    1. Open PowerShell or Windows Command Line in *administrator mode* by right-clicking and selecting "Run as administrator"
    2. Type in command line ```wsl --install``` and press enter
    3. Enter username, password and any necessary information
    4. Restart your machine
> You may need to assist yourself with some guide in case of any problems. Here is a good guide on how to install wsl on Windows: [link](https://human-se.github.io/rails-demos-n-deets-2020/demo-wsl/)
 2. Download all files from this repository
 3. Copy files to your linux machine
    1. Click Windows button and type Ubuntu, open the application
    2. Open File Explorer and in the path box enter: ```\\wsl.localhost\Ubuntu\home```
    3. Open directory with the name of user entered in the setup process
    4. Paste the unzipped directory containing our repository contents
    5. You may now close the File Explorer
 4. [Intall docker on Ubuntu machine.](https://docs.docker.com/engine/install/ubuntu/) Execute commands below in sequence.
    1. ```sudo apt-get update```
    2. ```sudo apt-get install ca-certificates curl gnupg```
    3. ```sudo install -m 0755 -d /etc/apt/keyrings```
    4. ```curl -fsSL https://download.docker.com/linux/ubuntu/gpg | sudo gpg --dearmor -o /etc/apt/keyrings/docker.gpg```
    5. ```sudo chmod a+r /etc/apt/keyrings/docker.gpg```
    6. ```echo "deb [arch=$(dpkg --print-architecture) signed-by=/etc/apt/keyrings/docker gpg] https://download.docker.com/linux/ubuntu $(. /etc/os-release && echo "$VERSION_CODENAME") stable" | sudo tee /etc/apt/sources.list.d/docker.list > /dev/null```
    7. ```sudo apt-get update```
    8. ```sudo apt-get install docker-ce docker-ce-cli containerd.io docker-buildx-plugin docker-compose-plugin```
 5. Verify that the Docker Engine installed succesfully by entering ```sudo docker run hello-world```
 6. Ensure docker has correct privileges by executing those commands:
    1. ```sudo groupadd docker```
    2. ```sudo usermod -aG docker ${USER}```
    3. ```sudo chmod 755 ~/MMM_webpage -R```
    4. ```sudo chown ${USER}:${USER} ~/MMM_webpage -R```
 7. You may now start docker containers containing our application.
    1. Go to "PrestaShop" directory inside project directory. ```cd ~/MMM_webpage/PrestaShop```
    2. Use docker compose to launch containers. ```docker compose up -d```
 8. If you followed those instructions step by step, you should have a working PrestaShop application connected to a mysql database and a phpMyAdmin tool for your database.
> If you encounter any problem you can try giving full privileges to the project files to everyone: ```sudo chmod 777 ~/MMM_webpage -R```. **It is not recommended in the production environments.**

## Api usage
 1. On your Ubuntu execute commands below in sequence: 
   1. ```sudo apt install python3-pip```, and type your super user password
   2. ```pip install prestapyt```
   3. ```pip install xmltodict```
 2. To start api script:
   1. ```cd api```
   2. ```python3 main.py```

## Included services
 1. PrestaShop website (Port 8080)
 2. Mysql database (Port 3306)
 3. phpMyAdmin tool (Port 8181)

## Software used
 1. PrestaShop ver. 1.7.8.10
 2. WSL ver. 1.2.5.0
 3. Ubuntu 22.04.2 LTS
 4. Docker ver. 24.0.7

## Composition of our team - MMM
 - Krzysztof Pecyna
 - Jakub Stachowicz
 - Marcin Stenka
 - Tim Schopinski
 - Kamil Kelsz